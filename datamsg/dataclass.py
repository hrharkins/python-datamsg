'''
#################
datamsg.dataclass
#################

An extension to dataclasses to streamline certain operations and reduce
boilerplate.  This class lays the foundation for datamsg.Message.

----------
The Basics
----------

DataClass provides access to dataclasses directly via subclassing.  This should
be used fro structural (support) classes rather than mainline to avoid surprise
but it's not impossible to apply to regular classes as well.

Normally a dataclass would be defined as follows (albeit less pedantically):

    >>> import dataclasses
    >>> @dataclasses.dataclass
    ... class MyClass(object):
    ...     x: int = 3
    ...     y: int = 7
    
    >>> MyClass(x=5)
    MyClass(x=5, y=7)
    
This is generally to be explicit (which is normally better than implicit), so
for many casses, this class would be inappropriate.

However, sometimes a hierarchy of classes needs to be built where dataclasses
needs to be implicit.  In these cases, using DataClass as a base provides
a foundation (esp. for DataMessage).

The above becomes:

    >>> import datamsg
    >>> class MyDataClass(datamsg.DataClass):
    ...     x: int = 3
    ...     y: int = 7

Subclasses automatically are also made into dataclasses.  Class members which
override fields are rewritten into fields with the appropriate default:

    >>> class MySubDataClass(MyDataClass):
    ...     y = 3
    ...     z: int = 9
    >>> MySubDataClass()
    MySubDataClass(x=3, y=3, z=9)
    >>> MySubDataClass(y=1)
    MySubDataClass(x=3, y=1, z=9)

Dataclass objects also can be called by default to reconfigure them.  This is
simply a shortcut to dataclasses.replace, so the same caveats apply.

    >>> d = MyDataClass()
    >>> d
    MyDataClass(x=3, y=7)
    >>> d(x=10)
    MyDataClass(x=10, y=7)

'''

import dataclasses, json
from datamsg.util import fix_dataclass_overrides

field = dataclasses.field

##############################################################################
##############################################################################

class DataClass(object):
    __dc_frozen__ = False
    __dc_init__ = True
    
    def __init_subclass__(cls, fix_overrides=fix_dataclass_overrides):
        fix_overrides(cls)
        kw = {}
        for name in ('frozen', 'init'):
            kw[name] = getattr(cls, '__dc_' + name + '__')
        dataclasses.dataclass(cls, **kw)

    def __call__(_self,  __replace__=dataclasses.replace, **_kw):
        return __replace__(_self, **_kw)
        
    class NoInit(object):
        __dc_init__ = False

    class Init(object):
        __dc_init__ = True

##############################################################################
##############################################################################

class FrozenDataClass(DataClass):
    __dc_frozen__ = True
