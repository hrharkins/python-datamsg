
from . import util, dataclass
import typing, io, sys

class Dispatcher(dataclass.DataClass):
    @util.cached('__call__')
    def build_fn(self, config=(), **_kw):
        config = dict(config, **_kw)
        stringify = self.build_stringify_fn(config)
        formatter = self.build_formatter_fn(stringify, config)
        return self.build_emitter_fn(formatter, config)

    def build_stringify_fn(self, config):
        return str

    def build_formatter_fn(self, str_fn, config):
        return str_fn
        
    def build_emitter_fn(self, str_fn, config):
        return str_fn                   # (DT-NO-COV)

class Emitter(Dispatcher):
    '''
        >>> class SetEmitter(Emitter):
        ...     dest: set = dataclass.field(default_factory=set)
        ...     def build_dest_fn(self, config):
        ...         return self.dest.add
        
        >>> s = SetEmitter(set())
        >>> s('Hello')
        >>> s.dest
        {'Hello'}
    '''
    
    def build_emitter_fn(self, str_fn, config):
        dest_fn = self.build_dest_fn(config)
        if dest_fn is None:
            return str_fn               # (DT-NO-COV)
        else:
            def emitter(msg, str_fn=str_fn, dest_fn=dest_fn):
                value = str_fn(msg)
                dest_fn(value)
            return emitter                

    def build_dest_fn(self, config):
        pass                            # (DT-NO-COV)

@util.delegate(
    'dest', 
    '__len__',
    '__iter__',
    '__getitem__',
    '__str__',
    '__repr__',
)
class ListEmitter(Emitter):
    '''
        >>> l = ListEmitter()
        >>> l('hello')
        >>> len(l)
        1
        >>> l
        ['hello']
        >>> l.dest
        ['hello']
    '''
    
    dest: list = dataclass.field(default_factory=list)
    
    def build_dest_fn(self, config):
        return self.dest.append

class FileEmitter(Emitter):
    '''
        >>> import io
        >>> out = io.StringIO()
        >>> FileEmitter(out)('Hello')
        >>> out.getvalue()
        'Hello'
    '''
    
    dest: io.IOBase
    mode: str = 'a'

    @util.cached
    def outfile(self):
        dest = self.dest
        if not isinstance(dest, io.IOBase):  # (DT-NO-COV)
            dest = open(dest, self.mode)
        assert dest.writable()            
        return dest
    
    def build_dest_fn(self, config):
        return self.outfile.write
        
class PrintEmitter(FileEmitter):
    '''
        >>> import io
        >>> out = io.StringIO()
        >>> PrintEmitter(out)('Hello')
        >>> out.getvalue()
        'Hello\\n'
    '''
    
    def build_dest_fn(self, config):
        return lambda s, o=self.outfile: print(s, file=o)

STDOUT = FileEmitter(sys.stdout)
STDERR = FileEmitter(sys.stderr)
