'''

    >>> from datamsg import DataMessage
    >>> class Greeting(DataMessage):
    ...     '[:: Hello, {.recipient} ::]'
    ...     recipient: str = 'world'
    
    >>> class EveryoneGreeting(Greeting):
    ...     recipient = 'everyone'
    
    >>> simple_log = Logger(print)
    >>> simple_log[Greeting]()
    
    >>> simple_log.enable(Greeting)
    >>> simple_log[Greeting]()
    Hello, world

    >>> tracker = []    
    >>> simple_log.enable(..., tracker.append)
    >>> simple_log[EveryoneGreeting]()
    Hello, everyone
    >>> tracker
    [EveryoneGreeting(recipient='everyone')]
    
    >>> tracker_a = []
    >>> tracker_b = []
    >>> dest_log = Logger(LOG_A=tracker_a.append, LOG_B=tracker_b.append)
    >>> dest_log.enable(Greeting, 'LOG_A')
    >>> dest_log.enable(EveryoneGreeting, 'LOG_B')
    >>> dest_log[Greeting]()
    >>> dest_log[EveryoneGreeting]()
    >>> tracker_a
    [Greeting(recipient='world'), EveryoneGreeting(recipient='everyone')]
    >>> tracker_b
    [EveryoneGreeting(recipient='everyone')]
    
'''

import sys, io
from . import util, dispatch, message

REFERENCE_TYPES = (str, type)
CONTAINER_TYPES = (tuple, frozenset)

def IGNORE(*_args, **_kw):
    pass

class Logger(dict):
    def __init__(self, *srcs_and_ellipsis_dests, unlogged=IGNORE, **enabled):
        self.enabled = {}
        self.unlogged = unlogged
        for src, dests in enabled.items():
            self.enable_src(src, dests)
        for src in srcs_and_ellipsis_dests:
            self.enable_src(..., src)

    def __missing__(self, src):
        dests = {}
        for dest in self.iter_dests_for_srcs(src):
            dests[id(dest)] = dest
        dests = list(dests.values())
        if len(dests) > 1:
            factory = src
            if isinstance(src, type):
                def fn(*_args, **_kw):
                    nonlocal factory, dests
                    msg = factory(*_args, **_kw)
                    for dest in dests:
                        dest(msg)
        elif len(dests):
            dest = dests[0]
            factory = src
            if isinstance(src, type):
                def fn(*_args, **_kw):
                    nonlocal factory, dest
                    dest(factory(*_args, **_kw))
        else:
            fn = self.unlogged
        self[src] = fn
        return fn

    def enable(self, src, *dests):
        if dests:
            self.enable_src(src, *dests)
        else:
            self.enable_src(src, ...)
    
    def enable_src(self, src, *dests):
        src_enabled = self.enabled.get(src)
        if src_enabled is None:
            src_enabled = self.enabled[src] = {}
        for dest in dests:            
            if isinstance(dest, CONTAINER_TYPES):
                self.enable(*dest)
            else:                
                src_enabled[id(dest)] = dest
        self.uncache(src)
        
    def uncache(self, src):
        if isinstance(src, type):
            uncache = set()
            for key in self:
                if isinstance(key, type) and issubclass(key, src):
                    uncache.add(key)
            for key in uncache:
                del self[key]
        else:
            self.pop(src, None)

    def iter_dests_for_srcs(self, *srcs):
        '''
        Helper for dests_for to do the heavy lifting of determinng dests for 
        the src objects specified.  Duplicate removal is handled by the
        caller if desired.

        Although part of the public API, this method is not expected to be
        used often and is primarily a helper for [] access.
        '''
        for src in srcs:
            if src is ... or isinstance(src, str):
                dests = self.enabled.get(src)
                if dests:
                    yield from self.iter_dests_for_dests(*dests.values())
            elif isinstance(src, type):
                for base in src.__mro__:
                    dests = self.enabled.get(base)
                    if dests:
                        yield from self.iter_dests_for_dests(*dests.values())
            else:
                raise InvalidSourceTypeError(logger=self, src=src)

    def iter_dests_for_dests(self, *dests, 
                             REFERENCE_TYPES=REFERENCE_TYPES,
                             CONTAINER_TYPES=CONTAINER_TYPES):
        '''
        Helper for iter_dests_for_srcs to do the heavy lifting of determinng 
        fanning out referenced destinations like strings or tuples, etc.

        Although part of the public API, this method is not expected to be
        used often and is primarily a helper for [] access.
        '''
        for dest in dests:
            if dest is ... or isinstance(dest, REFERENCE_TYPES):
                yield from self.iter_dests_for_srcs(dest)
            elif isinstance(dest, CONTAINER_TYPES):
                yield from self.iter_dests_for_dests(*dest)
            else:
                yield dest

class InvalidSourceTypeError(message.DataErrorMessage):
    '''
    [:: Cannot use {.src!r} as a means to log from {.logger!r} ::]
    
    [details::
    The {.src!r} provided is not a valid type as a logging source.  Valid types 
    are:
    
    * string
    * class
    * container (tuple, frozenset)
    ::]
    '''
    logger: Logger
    src: object
        