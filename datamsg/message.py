'''
################
datamasg.message
################

Simple message construction and stringification.

----------
The Basics
----------

Message (and its subtypes) allow for the simple declaration of formatted
messages built from attributes of their objects.  Although Message can
be used on any Python object or dataclass (via multiple inheritance), its 
simples form is DataMessaage, which combines it with the Data class defined 
elsewhere in datamsg.

For example:

    >>> class Greeting(DataMessage):
    ...     '[:: Hello, {.recipient} ::]'
    ...     recipient: str = 'world'
    
Defines a Greeting to be "Hello, {.recipient}", where by default recipient
is "world".  Since this a Data object (and also therefore a dataclass), the
following work similar to data classes:

    >>> Greeting()
    Greeting(recipient='world')
    >>> Greeting('everyone')
    Greeting(recipient='everyone')
    
But when converted to a string, for instance via print, the message is applied
to the members of the object:

    >>> print(Greeting())
    Hello, world
    >>> print(Greeting('everyone'))
    Hello, everyone

--------------------
Documenting Messages
--------------------

Note the [:: ::] part of the docstring.  Upon defintion, Message will examine
the first part of the docstring and if it has that pattern (and the message
is not defined for that class some other way), then the contents are made
into the message.  Pre/post spaces are removed and multiple lines are joined
with a space.  The intent is that this makes Message, especially errors,
easy to doucment and error messages can more easily be located in the code by 
their strings.

A more full-bodied version of the above:

    >>> class Greeting(DataMessage):
    ...     """
    ...     [:: Hello, {.recipient} ::]
    ... 
    ...     A greeting for a recipient, by default the world.
    ...     """
    ...
    ...     recipient: str = 'world'
    
Additionally, special detail messages can be added by specifying a name:

    >>> class Greeting(DataMessage):
    ...     """
    ...     [:: Hello, {.recipient} ::]
    ... 
    ...     [details::
    ...     A greeting for recipient {.recipient!r}.
    ...     ::]
    ...     """
    ...
    ...     recipient: str = 'world'
    
    >>> Greeting('you').details()
    "A greeting for recipient 'you'."

--------------
Raising Errors
--------------

Messages are *NOT* errors, so they cannot be raised as exceptions:

    >>> raise Greeting()
    Traceback (most recent call last):
        ...
    TypeError: exceptions must derive from BaseException

ErrorMessage (and DataErrorMessage) are based on Exception and can be raised:

    >>> class Problem(DataErrorMessage):
    ...     '[:: {.control}, we have a problem. ::]'
    ...     control: str = 'Houston'
    
    >>> raise Problem()
    Traceback (most recent call last):
        ...
    datamsg.message.Problem: Houston, we have a problem.
    
    >>> raise Problem('Mars')
    Traceback (most recent call last):
        ...
    datamsg.message.Problem: Mars, we have a problem.


Message will generate errors as needed, which are derived from a MessageFailure
base class.

--------------------------------
Using Messages without DataClass
--------------------------------

Although it's believed to be the most convenient, Message can be used without
DataClass (or even dataclasses.dataclass):

    >>> class PlainGreeting(Message):
    ...     '[:: Hello, {.recipient} ::]'
    ...     recipient='world'
    
    >>> print(PlainGreeting())
    Hello, world
    >>> print(PlainGreeting(recipient='everyone'))
    Hello, everyone

Or it can be mixed in with other classes:

    >>> class TypeErrorMessage(ErrorMessage, TypeError):
    ...     '[:: TypeError: {.args[0]} ::]'
    >>> print(TypeErrorMessage('hello'))
    TypeError: hello
   
Use with dataclasses is also fine:
    
    >>> import dataclasses
    >>> @dataclasses.dataclass
    ... class DCGreeting(Message):
    ...     '[:: Hello, {.recipient} ::]'
    ...     recipient: str = 'world'
    
    >>> DCGreeting()
    DCGreeting(recipient='world')

'''

from . import dataclass
import string, re, sys, os, io

##############################################################################
##############################################################################

MSG_RE = re.compile('''
    # Whitespace only.
    \s*
    
    # Opening bracket optionally contains the member to set:
    \[
        ([a-zA-Z_][a-zA-Z0-9_]*)?
    :
        # Future: other specifiers?  HTML, language code, etc???
    :\s*
    
    # Message portion:
    (.*?)
    
    # Closing bracket
    \s*::\]
''', re.M | re.S | re.X)
    
class Message(object):
    __msg_re__ = MSG_RE
    __formatter__ = string.Formatter()
    
    def __init__(self, *_args, **_kw):
        super().__init__(*_args)
        self.__dict__.update(**_kw)
        
    def __init_subclass__(cls):
        super().__init_subclass__()
        doc = cls.__dict__.get('__doc__')
        if doc is not None:
            for m in cls.__msg_re__.finditer(doc):
                varname, msgstr = m.groups()
                msg = cls.__docmsg__(msgstr)
                msgvar = '_' + ('_' + varname if varname else '') + '_msg__'
                if varname and not getattr(cls, varname, None):
                    def formatter(self):
                        nonlocal msgvar
                        msg = getattr(self, msgvar)
                        return self.__formatter__.vformat(msg, (), {'': self})
                    formatter.__name__ = varname
                    formatter.__doc__ = '[::' + msgstr + '::]'
                    setattr(cls, varname, formatter)
                if msgvar not in cls.__dict__:
                    setattr(cls, msgvar, msg)

    @classmethod                
    def __docmsg__(cls, text):
        lines = list(io.StringIO(text))
        prefix = os.path.commonprefix(lines[1:])
        lines[1:] = (line[len(prefix):].rstrip() for line in lines[1:])
        while lines and not lines[0]:
            del lines[0]
        while lines and not lines[-1]:
            del lines[-1]
        return '\n'.join(lines)

    @property
    def __msg__(self):
        raise NoMessageDefinedError(self)
        
    def __str__(self):
        return self.__formatter__.vformat(self.__msg__, (), {'': self})

##############################################################################

class ErrorMessage(Message, Exception):     # (COV-NO-DT)
    '''
    Base class for Errors that are also Messages.
    '''
    
##############################################################################
    
class DataMessage(Message, dataclass.DataClass): # (COV-NO-DT)
    '''
    Base class for messages combined with DataClass.
    '''

class DataErrorMessage(ErrorMessage, DataMessage): # (COV-NO-DT)
    '''
    Base class for errors combined with DataClass.
    '''

##############################################################################
##############################################################################

class MessageFailure(DataErrorMessage):
    pass

class InvalidGetItem(MessageFailure):
    '[:: Cannot use {.item!r} for {.type}[] ::]'
    item: object
    cls: object
    type: lambda s: s.cls.__name__

class CannotConvertError(MessageFailure):
    '[:: Cannot convert{.value} with {.cls!r} ::]'
    value: object
    self: object
    type: lambda s: s.cls.__name__

class NoMessageDefinedError(MessageFailure):
    '''
    [:: No message was defined for {.type!r} ::]
    
    Occurs if no message is defined either via __msg__ or [:: ... ::]
    
        >>> class Oops(Message):
        ...     pass
        >>> print(Oops())
        Traceback (most recent call last):
            ...
        datamsg.message.NoMessageDefinedError: No message was defined for 'Oops'

    '''
    self: object
    type = property(lambda s: type(s.self).__name__)

class FailureToStringifyError(MessageFailure):
    '''
    [:: {.error!r} during string conversion of {.msg!r} ::]
    '''
    msg: object
    error: object
    