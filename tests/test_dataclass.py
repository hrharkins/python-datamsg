
import unittest
import datamsg as dm

class TestDataClass(unittest.TestCase):
    def test_reconfigure(self):
        class MyClass(dm.DataClass):
            value: int = 3

        v = MyClass()
        self.assertEqual(v.value, 3)
        newv = v(value=7)
        self.assertEqual(newv.value, 7)
        
    def test_subclass(self):
        class MyClass(dm.DataClass):
            value: int = 3
            
        class MySubClass(MyClass):
            value: ... = 7
            
        self.assertEqual(MyClass().value, 3)            
        self.assertEqual(MySubClass().value, 7)

if __name__ == '__main__': # pragma: no cover
    unittest.main()
    