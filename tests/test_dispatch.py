
import unittest
import datamsg as dm
import io

class Greeting(dm.DataMessage):
    '<<< Hello, {.recipient} >>>'
    recipient: str = 'world'
    
class TestEmitters(unittest.TestCase):
    pass

if __name__ == '__main__': # pragma: no cover
    unittest.main()