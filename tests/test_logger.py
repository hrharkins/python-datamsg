
import unittest
import datamsg as dm
import io

class Greeting(dm.DataMessage):
    '<<< Hello, {.recipient} >>>'
    recipient: str = 'world'
    
class EveryoneGreeting(Greeting):
    recipient = 'everyone'
    
class TestLogger(unittest.TestCase):
    def test_ignore(self):
        unlogged = []
        log = dm.Logger(unlogged=lambda: unlogged.append(True))
        log[Greeting]()
        self.assertEqual(unlogged, [True])
        
    def test_simple_base(self):
        result = []
        log = dm.Logger(result.append)
        log.enable(Greeting)
        log[Greeting]()
        self.assertEqual(result, [Greeting()])
        
    def test_simple_subclass(self):
        result = []
        log = dm.Logger(result.append)
        log.enable(Greeting)
        log[EveryoneGreeting]()
        self.assertEqual(result, [EveryoneGreeting()])

    def test_subclass_only_log(self):
        result = []
        log = dm.Logger(result.append)
        log.enable(EveryoneGreeting)
        log[Greeting]()
        log[EveryoneGreeting]()
        self.assertEqual(result, [EveryoneGreeting()])
        
if __name__ == '__main__': # pragma: no cover
    unittest.main()
