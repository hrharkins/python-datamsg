
import unittest
import datamsg as dm

class TestMessage(unittest.TestCase):
    def test_trivial(self):
        self.assertEqual(
            str(self.GreetingMessage()),
            "Hello, world"
        )
        
    def test_argument(self):        
        self.assertEqual(
            str(self.GreetingMessage('everyone')),
            "Hello, everyone"
        )
        
    def test_subclass(self):
        class EveryoneGreetingMessage(self.GreetingMessage):
            recipient = 'everyone'

        self.assertEqual(
            str(EveryoneGreetingMessage()),
            "Hello, everyone"
        )
        
    def test_missing_msg(self):
        with self.assertRaises(dm.NoMessageDefinedError):
            str(dm.Message())

    class GreetingMessage(dm.DataMessage):
        '''
        [:: Hello, {.recipient} ::]
        
        Produces a greeting for te recipient specified, "world" by default.
        '''
        
        recipient: str = 'world'

    def _test_default_overide(self):

        class BaseMessage(dm.Message["I'm so {.something}"]):
            something: str = 'lazy'

        self.assertEqual(
            str(BaseMessage()),
            "I'm so lazy",
        )

        class SubMessage(BaseMessage):
            something = "sleepy"

        self.assertEqual(
            str(SubMessage()),
            "I'm so sleepy",
        )

    def _test_raise(self):
        with self.assertRaisesRegex(TypeError, 'derive from BaseException'):
            raise dm.Message()

        MyError = dm.DataErrorMessage['My lazy error!']

        with self.assertRaises(MyError, msg='My lazy error!'):
            raise MyError()

    def _test_type_convert(self):
        class ItsSoLazy(dm.DataErrorMessage):
            it: object

        self.assertEqual(
            str(ItsSoLazy['{.it!T} is so lazy!'](5)),
            "'int' is so lazy!",
        )

        self.assertEqual(
            str(ItsSoLazy['{.it!T} is so lazy!'](int)),
            "'type(int)' is so lazy!",
        )

        # Adm check that we didn't break regular conversions.
        class MyMessage(dm.Error['{.o!r}']):
            o: object

        self.assertEqual(str(MyMessage('hello')), "'hello'")

    def _test_lazy_override(self):
        class MyMessage(dm.Message["I'm so {.something}"]):
            something: str = 'lazy'

        self.assertEqual(
            str(MyMessage()),
            "I'm so lazy",
        )

        self.assertEqual(
            str(MyMessage[dict(something='sleepy')]()),
            "I'm so sleepy",
        )

        with self.assertRaises(dm.InvalidGetItem,
                               msg='Cannot use 12345 for Message[]'):
            dm.Message[12345]

    def _test_docstring_msg(self):
        class Simple(dm.Message):
            '[:: Hello world ::]'
        self.assertEqual(str(Simple()), 'Hello world')

        class SimpleMultiline(dm.Message):
            '''
            [:: Hello world ::]
            '''
        self.assertEqual(str(SimpleMultiline()), 'Hello world')

        class ExtraMultiline(dm.Message):
            '''
            [::

                Hello world

            ::]
            '''
        self.assertEqual(str(ExtraMultiline()), 'Hello world')

        class SplitLines(dm.Message):
            '''
            [::

                Hello

                world

            ::]
            '''
        self.assertEqual(str(ExtraMultiline()), 'Hello world')

        class WithOtherDocs(dm.Message):
            '''
            [:: Hello world ::]

            Appears whenever we feel like it.
            '''
        self.assertEqual(str(WithOtherDocs()), 'Hello world')

if __name__ == '__main__': # pragma: no cover
    unittest.main()
