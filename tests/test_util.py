
import unittest, random
import datamsg as dt

class TestCached(unittest.TestCase):
    def test_plain_decorator(self):
        class Adder(dt.DataClass):
            terms: tuple
            count: int = 0
            
            @dt.cached
            def computed(self):
                self.count += 1
                return sum(self.terms)

        a = Adder((3, 4, 5))                
        self.check_compute(a, 12)
        
    def test_called_decorator(self):
        class Adder(dt.DataClass):
            terms: tuple
            count: int = 0
            
            @dt.cached()
            def computed(self):
                self.count += 1
                return sum(self.terms)

        a = Adder((3, 4, 5))                
        self.check_compute(a, 12)
        
    def test_named_decorator(self):
        class Adder(dt.DataClass):
            terms: tuple
            count: int = 0
            
            @dt.cached('computed')
            def compute(self):
                self.count += 1
                return sum(self.terms)

        a = Adder((3, 4, 5))                
        self.check_compute(a, 12)
        
    def test_get_cached_object(self):
        class Adder(dt.DataClass):
            terms: tuple
            count: int = 0
            
            @dt.cached('computed')
            def compute(self):
                self.count += 1
                return sum(self.terms)

        self.assertEqual(type(Adder.computed).__name__, 'computed')

    def check_compute(self, obj, check):
        for m in range(int(random.random() * 12 + 12)):
            self.assertEqual(obj.count, m)
            for n in range(int(random.random() * 12 + 12)):
                self.assertEqual(obj.computed, check)
                self.assertEqual(obj.count, m + 1)
            del obj.__cache__.computed

if __name__ == '__main__':  # pragma: no cover
    unittest.main()
